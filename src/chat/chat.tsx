import React from 'react';
import Header from "../header/header";
import {MessageList} from "../message/message-list/message-list";
import {MessageType} from "../common/type/message-type";
import {http} from "../common/http/http";
import {Preloader} from "../preloader/preloader";
import {MessageInput} from "../message/message-input/message-input";
import {countMessagesByKey, lastMessageDate} from "../message/message-helper";
import {UserType} from "../common/type/user-type";
import { v4 as uuid } from 'uuid';
import './chat.css';

type ChatState = {
    title: string;
    loggedUser: UserType;
    messages: Array<MessageType>;
    lastMessageDate?: Date;
    usersCount?: number;
    messagesCount?: number;
    loading: boolean;
    error?: Error;
    editMessage?: MessageType;
}

type ChatProps = {
    url: URL;
}

class Chat extends React.Component<ChatProps, ChatState> {
    constructor(props: ChatProps) {
        super(props);
        this.state = {
            title: "BSA Chat",
            loggedUser: {
                id: uuid().toString(),
                avatar: "https://flic.kr/ps/3WVVQB",
                user: "Tamara"
            },
            messages: [],
            loading: false
        }
    }

    componentDidMount() {
        this.setState({loading: true}, () => {
            http<MessageType[]>(this.props.url.toString())
                .then(response => {
                    const messages = response.parsedBody ? [...response.parsedBody] : [];
                    return this.setState({
                        loading: false,
                        messages: messages,
                        lastMessageDate: lastMessageDate(messages),
                        usersCount: countMessagesByKey(messages, "userId"),
                        messagesCount: countMessagesByKey(messages, "id"),
                        error: undefined
                    })
                }).catch(e => {
                this.setState({
                    loading: false,
                    error: new Error(`Error happened during fetching messages: ${e}`)
                });
            });
        });
    }

    addNewMessageHandle = (text: string) => {
        const {loggedUser} = this.state;

        const newMessage = {
            id: uuid().toString(),
            text: text,
            userId: loggedUser.id,
            avatar: loggedUser.avatar,
            user: loggedUser.user,
            liked: false,
            createdAt: new Date()
        }
        const newMessages = [...this.state.messages, newMessage];
        this.setState((state) => {
            return {
                messages: newMessages,
                lastMessageDate: newMessage.createdAt,
                messagesCount: state.messagesCount ? state.messagesCount + 1 : 1,
                usersCount: countMessagesByKey(newMessages, "userId")
            }
        });
    }

    onEdit = (id: string) => {
        const message = this.findMessage(id);
        if (message) {
            this.setState({editMessage: message});
        }
    }

    editMessageHandle = (id: string, text: string) => {
        const message = this.findMessage(id);
        if (message) {
            message.text = text;
            message.updatedAt = new Date();
        }
        this.setState((state) => {
            return {
                messages: state.messages,
                editMessage: undefined
            }
        });
    }

    sendMessageHandle = (id: string | undefined, text: string) => {
        if (id) {
            this.editMessageHandle(id, text);
        } else {
            this.addNewMessageHandle(text);
        }
    }

    deleteHandle = (id: string) => {
        const newMessages = [...this.state.messages];
        const message = this.findMessageInArray(id, newMessages);
        if (!message) {
            return;
        }
        const index = newMessages.indexOf(message);
        if (index > -1) {
            newMessages.splice(index, 1);
        }
        this.setState((state) => {
            return {
                messages: newMessages,
                lastMessageDate: lastMessageDate(newMessages),
                messagesCount: state.messagesCount ? state.messagesCount - 1 : 0,
                usersCount: countMessagesByKey(newMessages, "userId")
            }
        });
    }

    likeHandle = (id: string, liked: boolean) => {
        const message = this.findMessage(id);
        if (message) {
            message.liked = liked;
        }
        this.setState((state) => {
            return {
                messages: state.messages
            }
        });
    }

    findMessage(id: string): MessageType | undefined {
        return this.findMessageInArray(id, this.state.messages);
    }

    findMessageInArray(id: string, messages: Array<MessageType>): MessageType | undefined {
        return messages.find(i => i.id === id);
    }

    render() {
        const {
            loggedUser, messages, loading, error, messagesCount, lastMessageDate,
            title, usersCount, editMessage
        } = this.state;

        return (<div className="chat">
            <Header title={title}
                    messagesCount={messagesCount}
                    usersCount={usersCount}
                    lastMessageDate={lastMessageDate}/>
            <MessageList messages={messages}
                         loggedUserId={loggedUser.id}
                         onEdit={this.onEdit}
                         onDelete={this.deleteHandle}
                         onLike={this.likeHandle}/>
            {error && <div className="ui error message">{error.message}</div>}
            <MessageInput id={editMessage?.id}
                          text={editMessage?.text}
                          onSend={this.sendMessageHandle}/>
            {loading && <Preloader/>}
        </div>);
    }
}

export default Chat;
