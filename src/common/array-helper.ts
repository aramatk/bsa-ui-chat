export function sortByKey<T extends object, K extends keyof T>(array: Array<T>, key: K) {
    return [...array].sort(function (a, b) {
        const x: any = a[key];
        const y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

export const getKeyValue = <T extends object, K extends keyof T>(key: K) => (obj: T) =>
    obj[key];

export interface KeyGetter<T> {
    (obj: T): string;
}

export function groupBy<T extends object>(list: Array<T>, keyGetter: KeyGetter<T>)
    : Map<string, Array<T>> {
    const map = new Map<string, Array<T>>();
    list.forEach((item) => {
        const key = keyGetter(item);
        const array = map.get(key);
        if (!array) {
            map.set(key, [item]);
        } else {
            array.push(item);
        }
    });
    return map;
}