export interface MessageType {
    id:string;
    userId:string;
    text: string;
    createdAt: Date;
    updatedAt?: Date;
    avatar: string;
    user: string;
    liked: boolean;
}