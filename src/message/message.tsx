import React, {FunctionComponent} from 'react';
import moment from 'moment';
import './message.css';
import {Card} from "semantic-ui-react";

type  MessageProps = {
    id: string;
    text: string;
    createdAt: Date;
    liked: boolean;
    avatarUrl: string;
    userName: string;
    onLike: (id: string, like: boolean) => void;
}

export const Message: FunctionComponent<MessageProps> =
    ({id, text, createdAt, liked = false, avatarUrl, userName, onLike}) => {

        const handleLike = () => {
            onLike(id, !liked);
        }

        return (<Card className="message" key={id}>
            <Card.Content>
                <Card.Meta>
                    <span className="message-time">{moment(createdAt).format("HH:mm")}</span>
                </Card.Meta>
                <Card.Description>
                    <p className="message-text">{text}</p>
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                    <span className="left floated">
                        <i className={`message-like heart icon ${liked ? 'message-liked' : ''}`}
                           onClick={handleLike}></i>
                    </span>
                <div className="right floated author">
                    <img className="message-user-avatar ui avatar image" src={avatarUrl} alt={userName}/>
                    <span className="message-user-name">{userName}</span>
                </div>
            </Card.Content>
        </Card>);
    }