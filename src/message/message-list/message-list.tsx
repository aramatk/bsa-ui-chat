import React, {FunctionComponent} from 'react';
import {MessageType} from "../../common/type/message-type";
import {Message} from "../message";
import {OwnMessage} from "../own-message/own-message";
import {groupMessages} from "../message-helper";

import './message-list.css';
import {Segment} from "semantic-ui-react";

type  MessageListProps = {
    loggedUserId: string
    messages: Array<MessageType>;
    onEdit: (id: string) => void;
    onDelete: (id: string) => void;
    onLike: (id: string, liked: boolean) => void;
}

export const MessageList: FunctionComponent<MessageListProps> = ({
                                                                     messages,
                                                                     loggedUserId, onEdit, onDelete, onLike
                                                                 }) => {
    const createMessageElement = (m: MessageType): JSX.Element => {
        if (m.userId === loggedUserId) {
            return <OwnMessage id={m.id}
                               text={m.text}
                               createdAt={m.createdAt}
                               onEdit={onEdit}
                               onDelete={onDelete}/>
        } else {
            return (
                <Message id={m.id}
                         text={m.text}
                         createdAt={m.createdAt}
                         liked={m.liked}
                         avatarUrl={m.avatar}
                         userName={m.user}
                         onLike={onLike}/>
            );
        }
    }

    return (
        <Segment className="message-list">
            {Array.from(groupMessages(messages)).map(
                ([key, value]) =>
                    <div key={key}>
                        <div className="messages-divider ui horizontal divider">{key}</div>
                        {value.map((m) => <div key={m.id}>{createMessageElement(m)}</div>)}
                    </div>
            )}
        </Segment>
    );
}