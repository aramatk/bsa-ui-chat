import React, {FunctionComponent} from 'react';
import moment from 'moment';
import './own-message.css';
import {Card} from "semantic-ui-react";

type  OwnMessageProps = {
    id: string;
    text: string;
    createdAt: Date;
    onEdit: (id: string) => void;
    onDelete: (id: string) => void;
}

export const OwnMessage: FunctionComponent<OwnMessageProps> =
    ({id, text, createdAt, onEdit, onDelete}) => {

        const handleEdit = () => {
            onEdit(id);
        }
        const handleDelete = () => {
            onDelete(id);
        }

        return (<Card className="own-message" key={id}>
            <Card.Content>
                <Card.Meta>
                    <span className="message-time">{moment(createdAt).format("HH:mm")}</span>
                </Card.Meta>
                <Card.Description>
                    <p className="message-text">{text}</p>
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <span className="right floated">
                     <i className="edit icon message-edit" onClick={handleEdit}></i>
                     <i className="delete icon message-delete" onClick={handleDelete}></i>
              </span>
            </Card.Content>
        </Card>);
    }