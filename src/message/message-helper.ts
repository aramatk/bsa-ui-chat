import moment from "moment";
import {MessageType} from "../common/type/message-type";
import {getKeyValue, groupBy, sortByKey} from "../common/array-helper";

export enum Day {
    Today = "Today",
    Yesterday = "Yesterday"
}

const formatDate = (date: Date): string => {
    if (moment(date).isSame(moment(), "day")) {
        return Day.Today;
    } else if (moment(date).isSame(moment().subtract(1, 'day'), "day")) {
        return Day.Yesterday;
    } else {
        return moment(date).format('dddd, DD MMMM');
    }
}

export const groupMessages = (messages: Array<MessageType>): Map<string, Array<MessageType>> => {
    if (messages && messages.length > 0) {
        return groupBy(sortByKey(messages, "createdAt"),
            (m: MessageType) => formatDate(m.createdAt));
    } else {
        return new Map<string, Array<MessageType>>();
    }
}

export function countMessagesByKey<K extends keyof MessageType>(messages: Array<MessageType>, key: K): number {
    if (messages && messages.length > 0) {
        return new Set(messages.map(i => getKeyValue<MessageType, K>(key)(i))).size;
    } else {
        return 0;
    }
}

export const lastMessageDate = (messages: Array<MessageType>): Date | undefined => {
    if (messages && messages.length > 0) {
        const sortedArray = sortByKey(messages, "createdAt");
        return sortedArray[sortedArray.length - 1].createdAt;
    }
}