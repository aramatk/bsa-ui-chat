import React from 'react';
import './message-input.css';
import {Form, Segment} from "semantic-ui-react";

type  MessageInputProps = {
    id?: string;
    text?: string;
    onSend: (id: string | undefined, text: string) => void;
}

type MessageInputState = {
    id?: string;
    text?: string;
}

export class MessageInput extends React.Component<MessageInputProps, MessageInputState> {

    constructor(props: MessageInputProps) {
        super(props);
        this.state = {
            text: props.text,
            id: props.id
        }
    }

    handleSend = () => {
        if (!this.state.text) {
            return;
        }
        this.props.onSend(this.props.id, this.state.text);
        this.setState({text: "", id: undefined});
    };

    onChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
        const newValue = e.currentTarget.value;
        this.setState({text: newValue});
    }

    static getDerivedStateFromProps(nextProps: Readonly<MessageInputProps>, prevState: Readonly<MessageInputState>) {
        if (nextProps.text && nextProps.id && prevState.id !== nextProps.id) {
            return {
                text: nextProps.text,
                id: nextProps.id
            };
        } else {
            return null;
        }
    }

    render() {
        return (<Segment className="message-input">
            <Form onSubmit={this.handleSend} className="message-form">
                <textarea
                    className="message-input-text"
                    value={this.state.text}
                    onChange={this.onChange}
                />
                <button className="message-input-button ui blue labeled submit icon button"
                        type='submit'
                        onClick={this.handleSend}>
                    <i className="icon send"></i> Send
                </button>
            </Form>
        </Segment>);
    }
}