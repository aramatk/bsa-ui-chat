import React from 'react';
import moment from 'moment';
import './header.css';
import {Grid} from "semantic-ui-react";

type  HeaderProps = {
    title: string;
    usersCount?: number;
    messagesCount?: number;
    lastMessageDate?: Date;
}

const Header = ({title, usersCount = 0, messagesCount = 0, lastMessageDate}: HeaderProps) => (
    <Grid className="header segment" columns="3" verticalAlign="middle">
        <Grid.Column>
            <span className="header-title">{title}</span>
        </Grid.Column>
        <Grid.Column textAlign="right" className="count-info">
            <span className="header-users-count">{usersCount}</span> participants
            <br/>
            <span className="header-messages-count">{messagesCount}</span> messages
        </Grid.Column>
        <Grid.Column textAlign="right" className="header-date">
            {lastMessageDate &&
            <span><span>Last message at </span><span
                className="header-last-message-date">{moment(lastMessageDate).format("DD.MM.yyyy HH:mm")}
            </span></span>}
        </Grid.Column>
    </Grid>
)

export default Header;
