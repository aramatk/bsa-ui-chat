import React from 'react';
import ReactDOM from 'react-dom';

import './styles/common.css';
import 'semantic-ui-css/semantic.min.css';
import Chat from "./chat/chat";

ReactDOM.render(
    <React.StrictMode>
        <Chat url={new URL("https://edikdolynskyi.github.io/react_sources/messages.json")}/>
    </React.StrictMode>,
    document.getElementById('root')
);