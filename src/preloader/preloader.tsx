import React, {FunctionComponent} from 'react';

export const Preloader: FunctionComponent = () => (
    <div className="preloader ui large text loader active">Loading</div>
);