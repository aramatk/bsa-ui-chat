Нужно написать приложение Чат. User Iinterface чата должен содержать следующие компоненты с классами элементов указанных в скобках:

###Header 
отдельный компонент (.header), который содержит название чата (.header-title), количество пользователей (.header-users-count), количество сообщений (.header-messages-count), дата последнего сообщения (.header-last-message-date) в формате dd.mm.yyyy hh:mm (Например: 15.02.2021 13:35)

###Message
Компонент сообщения (.message), должен содержать текст (.message-text), время (.message-time) в формате hh:mm, кнопку “лайк” (.message-like или .message-liked), имя пользователя (.message-user-name) и аватар (.message-user-avatar).

###OwnMessage
Компонент личного сообщения (.own-message), содержит текст (.message-text), время (.message-time) в формате hh:mm, кнопки редактирования (.message-edit) и удаления (.message-delete)

###MessageList
Компонент который содержит список сообщений (.message-list). Список должен быть разделен по дням (.messages-divider) (линия с указанным днем, в формате "Today", "Yesterday", "Monday, 17 June")

###MessageList
Компонент (.message-input), который содержит текстовое поле (.message-input-text) и кнопку "Send" (.message-input-button). Должен использоваться для отправки и редактирования сообщения.

###Preloader 
Компонент со спиннером предзагрузки данных (.preloader)

###Chat 
Компонент (.chat), который принимает URL ресурс с данными для чата и отрисовывает все вышеперечисленные компоненты. Это компонент должен иметь следующий формат: